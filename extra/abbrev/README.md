# Journal Name Abbreviations

Journal lists:

* https://www.library.caltech.edu/journal-title-abbreviations
* https://images.webofknowledge.com/images/help/WOS/A_abrvjt.html

```
$ pup 'dl json{}' < A_abrvjt.html | jq -rc .[0].children[] | grep "children" | python parse.py
```

Generate a name mapping as json.

```
$ while read line;
    do (curl -v "$line" | pup 'dl json{}' | jq -rc .[0].children[] | grep "children" | python parse.py);
done < <(curl "https://www.library.caltech.edu/journal-title-abbreviations" | pup 'a attr{href}' | grep "help/WOS") > result.json
```

Output example (pretty printed):

```json
{
  "name": "JOURNAL OF COMPARATIVE PATHOLOGY",
  "abbrev": "J COMP PATHOL"
}
{
  "name": "CIRCULAR-GRATING LIGHT-EMITTING SOURCES",
  "abbrev": "P SOC PHOTO-OPT INS"
}
{
  "name": "PEST CONTROL &amp; SUSTAINABLE AGRICULTURE"
}
{
  "name": "COMPUTER INFORMATION SYSTEMS AND INDUSTRIAL MANAGEMENT",
  "abbrev": "LECT NOTES COMPUT SC"
}
{
  "name": "Flexible Databases Supporting Imprecision and Uncertainty",
  "abbrev": "STUD FUZZ SOFT COMP"
}
```
