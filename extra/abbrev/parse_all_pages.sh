#!/bin/bash
#
# Scrape abbreviations from https://www.library.caltech.edu/journal-title-abbreviations
#
# We may want these for reference snooping.

set -e -u -o pipefail

for cmd in curl pup jq python; do
	command -v "$cmd" >/dev/null 2>&1 || {
		echo >&2 "missing $cmd"
		exit 1
	}
done

while read line; do
	(
		echo >&2 "$line"
		sleep 1
		curl -sL "$line" |
			pup 'dl json{}' |
			jq -rc .[0].children[] |
			grep "children" |
			python parse_dls.py
	)
done < <(curl -sL "https://www.library.caltech.edu/journal-title-abbreviations" |
	pup 'a attr{href}' |
	grep "help/WOS")
