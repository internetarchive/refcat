package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	var (
		cache = make(map[int]string)
		br    = bufio.NewReader(os.Stdin)
		i     int
	)
	for {
		line, err := br.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		fields := strings.Split(line, "\t")
		if len(fields) < 2 {
			continue
		}
		a, b := strings.TrimSpace(fields[0]), strings.TrimSpace(fields[1])
		id, err := strconv.Atoi(a)
		if err != nil {
			log.Println("skipping invalid id: %s", line)
			continue
		}
		cache[id] = b
		i++
		if i%1000000 == 0 {
			log.Printf("%d", i)
		}
	}
	log.Println("press enter to quit")
	fmt.Scanln()
}
