# Misc

* on blocking: http://www.dlib.org/dlib/november14/fedoryszak/11fedoryszak.html

> In  contrast  with  these  other  services,  however, the  WoS  uniquely
> offers  ‘author/source’  citation  search-ing  which,  with  some  diligence,
> also  uniquely  retrieves mis-cited or in-press references.

From: https://authors.library.caltech.edu/24838/1/ROTcs05.pdf
