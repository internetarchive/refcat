# Utilities, e.g. for printing dependency tree.

import collections


def dump_deps(task=None, indent=0):
    """
    Print dependency graph for a given task to stdout.
    """
    if task is None:
        return
    g = build_dep_graph(task)
    print('%s \_ %s' % ('   ' * indent, task))
    for dep in g[task]:
        dump_deps(task=dep, indent=indent + 1)


def dump_deps_dot(task=None):
    if task is None:
        return
    g = build_dep_graph(task)
    print("digraph deps {")
    print("node [shape=record];")
    for k, vs in g.items():
        for v in vs:
            print(""" "{}" -> "{}"; """.format(k, v))
    print("}")


def build_dep_graph(task=None):
    """
    Return the task graph as dict mapping nodes to children.
    """
    g = collections.defaultdict(set)
    queue = [task]
    while len(queue) > 0:
        task = queue.pop()
        for dep in task.deps():
            g[task].add(dep)
            queue.append(dep)
    return g
