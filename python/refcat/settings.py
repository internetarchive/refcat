from pathlib import Path

from dynaconf import Dynaconf

# logging configuration
LOGGING_CONF_FILE = str(Path.home().joinpath(".config/refcat/logging.ini"))

# application settings
CONFIG_FILE = str(Path.home().joinpath(".config/refcat/settings.ini"))
ENVVAR_PREFIX_FOR_DYNACONF = "REFCAT"
ENV_FOR_DYNACONF = "default"

settings = Dynaconf(ENVIRONMENTS=True,
                    ENV_FOR_DYNACONF=ENV_FOR_DYNACONF,
                    ENVVAR_PREFIX_FOR_DYNACONF=ENVVAR_PREFIX_FOR_DYNACONF,
                    SETTINGS_FILE_FOR_DYNACONF=CONFIG_FILE)
