import setuptools

from refcat import __version__

with open("README.md", "r") as fh:
    long_description = fh.read()

    setuptools.setup(
        name="refcat",
        version=__version__,
        author="Martin Czygan",
        author_email="martin@archive.org",
        description="Reference data munging tasks and utilities",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.com/internetarchive/cgraph",
        packages=setuptools.find_packages(),
        classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ],
        python_requires=">=3.6",
        entry_points={"console_scripts": [
            "refcat=refcat.cli:main"
        ]},
        install_requires=[
            "dynaconf[ini]",
            "grobid-tei-xml",
            "luigi",
            "requests",
        ],
        extras_require={"dev": [
            "ipython",
            "isort",
            "mypy",
            "pylint",
            "pytest",
            "pytest-cov",
            "shiv",
            "twine",
            "yapf",
        ],},
    )
