package skate

import (
	"bytes"
	"testing"
)

func TestCache(t *testing.T) {
	c := &Cache{Dir: t.TempDir()}
	if c.Has("k") {
		t.Fatalf("empty cache should not have any value")
	}
	w := []byte("vvvv")
	err := c.Set("k", w)
	if err != nil {
		t.Fatalf("set: %v", err)
	}
	if !c.Has("k") {
		t.Fatalf("key should exist")
	}
	v, err := c.Get("k")
	if err != nil {
		t.Fatalf("get: %v", err)
	}
	if !bytes.Equal(v, w) {
		t.Fatalf("got %v, want %v", v, w)
	}
}
