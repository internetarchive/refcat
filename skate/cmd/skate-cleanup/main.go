// Filter to parse out a correctly looking DOI, URL, etc from a field.
//
// $ echo "1,xxx 10.123/12312 xxx,3" | skate-to-doi -c doi -d , -f 2
// 1,10.123/12312,3k
//
// We can use this to sanitize fields in the reference dataset.

package main

import (
	"flag"
	"log"
	"os"
	"runtime"
	"strings"

	"gitlab.com/internetarchive/refcat/skate"
	"gitlab.com/internetarchive/refcat/skate/parallel"
)

var (
	numWorkers      = flag.Int("w", runtime.NumCPU(), "number of workers")
	batchSize       = flag.Int("b", 100000, "batch size")
	delimiter       = flag.String("d", "\t", "delimiter")
	index           = flag.Int("f", 1, "one field to cleanup up a doi, 1-indexed")
	bestEffort      = flag.Bool("B", false, "only log errors, but do not stop")
	skipNonMatches  = flag.Bool("S", false, "do not emit a line for non-matches")
	what            = flag.String("c", "doi", "what to clean: doi, url, ref")
	extendedCleanup = flag.Bool("X", false, "extended (and slower) cleanup for urls")
	allow           = flag.String("allow", "http,https", "comma separted list of schemas to allow for urls")

	allowedSchemas []string // parsed from allow flag
)

func main() {
	flag.Parse()
	for _, v := range strings.Split(*allow, ",") {
		allowedSchemas = append(allowedSchemas, strings.TrimSpace(v))
	}
	var f func([]byte) ([]byte, error)
	switch *what {
	case "ref":
		filter := skate.FilterRawRef{}
		f = filter.Run
	case "url":
		filter := skate.FilterURL{
			Delimiter:      *delimiter,
			Index:          *index,
			BestEffort:     *bestEffort,
			Aggressive:     *extendedCleanup,
			SkipNonMatches: *skipNonMatches,
			AllowedSchemas: allowedSchemas,
		}
		f = filter.Run
	default:
		filter := skate.FilterDOI{
			Delimiter:      *delimiter,
			Index:          *index,
			BestEffort:     *bestEffort,
			Aggressive:     *extendedCleanup,
			SkipNonMatches: *skipNonMatches,
		}
		f = filter.Run
	}
	pp := parallel.NewProcessor(os.Stdin, os.Stdout, f)
	pp.NumWorkers = *numWorkers
	pp.BatchSize = *batchSize
	if err := pp.Run(); err != nil {
		log.Fatal(err)
	}
}
