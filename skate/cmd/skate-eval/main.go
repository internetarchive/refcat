// skate-eval generates a match report for source and target citation
// documents. We take a source and target and will try to verify the edge.
//
// Reads tabular data from stdin and will output tabular data to stdout.
//
// TODO: keep only letters, remove everything else
package main

import (
	"bufio"
	"crypto/sha1"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strings"
	"sync/atomic"
	"time"
	"unicode"

	"github.com/sethgrid/pester"
	"golang.org/x/text/unicode/norm"
)

var (
	fatcatApi = flag.String("api", "https://api.fatcat.wiki/v0", "fatcat API endpoint")
	timeout   = flag.Duration("t", 15*time.Second, "request timeout")
	atLeastN  = flag.Uint64("n", 100, "stop after n successful checks")
)

// Getter to plugin in different HTTP client implementation.
type Getter interface {
	Get(string) (*http.Response, error)
}

// File information.
type File struct {
	Extra struct {
		Shadows struct {
			ScimagDoi string `json:"scimag_doi"`
			ScimagId  string `json:"scimag_id"`
		} `json:"shadows"`
	} `json:"extra"`
	Ident      string   `json:"ident"`
	Md5        string   `json:"md5"`
	Mimetype   string   `json:"mimetype"`
	ReleaseIds []string `json:"release_ids"`
	Revision   string   `json:"revision"`
	Sha1       string   `json:"sha1"`
	Sha256     string   `json:"sha256"`
	Size       int64    `json:"size"`
	State      string   `json:"state"`
	Urls       []struct {
		URL string `json:"url"`
		Rel string `json:"rel"`
	} `json:"urls"`
}

// ReleaseFiles fatcat API response for release including file information.
type ReleaseFiles struct {
	Abstracts   []interface{} `json:"abstracts"`
	ContainerId string        `json:"container_id"`
	Contribs    []struct {
		Extra struct {
			Seq string `json:"seq"`
		} `json:"extra"`
		Index   int64  `json:"index"`
		RawName string `json:"raw_name"`
		Role    string `json:"role"`
	} `json:"contribs"`
	ExtIds struct {
		Doi string `json:"doi"`
	} `json:"ext_ids"`
	Extra struct {
		Crossref struct {
			Type string `json:"type"`
		} `json:"crossref"`
	} `json:"extra"`
	Files        []File        `json:"files"`
	Ident        string        `json:"ident"`
	Pages        string        `json:"pages"`
	Publisher    string        `json:"publisher"`
	Refs         []interface{} `json:"refs"`
	ReleaseDate  string        `json:"release_date"`
	ReleaseStage string        `json:"release_stage"`
	ReleaseType  string        `json:"release_type"`
	ReleaseYear  int64         `json:"release_year"`
	Revision     string        `json:"revision"`
	State        string        `json:"state"`
	Title        string        `json:"title"`
	Volume       string        `json:"volume"`
	WorkId       string        `json:"work_id"`
}

// FindUsableURL returns the first non-archive URL found or an empty string, otherwise.
func FindUsableURL(doc *ReleaseFiles) (link string) {
	if len(doc.Files) == 0 {
		return
	}
	for _, f := range doc.Files {
		if len(f.Urls) == 0 {
			continue
		}
		for _, u := range f.Urls {
			if u.Rel == "webarchive" {
				continue
			}
			link = u.URL
			return
		}
	}
	return
}

func Truncate(s string, l int) string {
	if len(s) < l {
		return s
	}
	return s[:l] + "..."
}

func FirstLine(s string) string {
	lines := strings.Split(s, "\n")
	return lines[0]
}

// FetchReleaseFile fetches a release with file information from the fatcat API
// with a given client.
func FetchReleaseFile(g Getter, releaseIdent string) (*ReleaseFiles, error) {
	link := fmt.Sprintf("%s/release/%s?expand=files", *fatcatApi, releaseIdent)
	fn, err := CacheFileLocally(g, link)
	if err != nil {
		return nil, err
	}
	f, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var payload ReleaseFiles
	if err := json.NewDecoder(f).Decode(&payload); err != nil {
		return nil, err
	}
	return &payload, nil
}

// CacheFileLocally retrieves a URL and stores the content locally. Returns the path of the local file.
func CacheFileLocally(g Getter, link string) (string, error) {
	h := sha1.New()
	_, _ = h.Write([]byte(link))
	var (
		hsum       = fmt.Sprintf("%x", h.Sum(nil))
		targetFn   = fmt.Sprintf("skate-eval-download-%s.pdf", hsum)
		targetPath = path.Join(os.TempDir(), targetFn)
		tmpFile    *os.File
	)
	if _, err := os.Stat(targetPath); os.IsNotExist(err) {
		resp, err := g.Get(link)
		if err != nil {
			return "", err
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			return "", fmt.Errorf("got http %v from %s", resp.StatusCode, link)
		}
		tmpFile, err = os.Create(targetPath)
		if err != nil {
			return "", fmt.Errorf("error creating temporary file: %v", err)
		}
		defer tmpFile.Close()
		_, err = io.Copy(tmpFile, resp.Body)
		if err != nil {
			return "", fmt.Errorf("copy: %w", err)
		}
		if _, err := tmpFile.Seek(0, io.SeekStart); err != nil {
			return "", err
		}
		return tmpFile.Name(), nil
	} else {
		return targetPath, nil
	}
}

func isAsciiOnly(s string) bool {
	for c := range s {
		if c > 128 {
			return false
		}
	}
	return true
}

// lettersOnly keeps only the letters of the string
func lettersOnly(s string) string {
	var b strings.Builder
	for _, c := range s {
		if !unicode.IsLetter(c) {
			continue
		}
		_, _ = b.WriteRune(c)
	}
	return b.String()
}

func main() {
	flag.Parse()
	var (
		i, hits atomic.Uint64
		br      = bufio.NewReader(os.Stdin)
		client  = pester.New()
		done    = false
	)
	client.Concurrency = 3
	client.MaxRetries = 1
	client.Timeout = *timeout
	client.Backoff = pester.ExponentialBackoff
	client.KeepLog = true
	var sem = make(chan bool, 100)
	for {
		if done {
			break
		}
		line, err := br.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		sem <- true
		go func() {
			defer func() {
				_ = <-sem
			}()
			fields := strings.Fields(line)
			if len(fields) < 2 {
				log.Fatalf("expected at least two fields, found: %v", len(fields))
			}
			sourceReleaseIdent := fields[0]
			targetReleaseIdent := fields[1]
			if len(sourceReleaseIdent) != 26 || len(targetReleaseIdent) != 26 {
				return
			}
			// Fetch releases.
			sr, err := FetchReleaseFile(client, sourceReleaseIdent)
			if err != nil {
				log.Printf("fetch: %v", err)
				return
			}
			tr, err := FetchReleaseFile(client, targetReleaseIdent)
			if err != nil {
				log.Printf("fetch: %v", err)
				return
			}
			// try to find usable URL for source and download source document
			sourceDocLink := FindUsableURL(sr)
			if sourceDocLink == "" {
				// log.Printf("skip: no url found")
				return
			}
			fn, err := CacheFileLocally(client, sourceDocLink)
			if err != nil {
				return
			}
			f, err := os.Open(fn)
			if err != nil {
				return
			}
			defer f.Close()

			cmd := exec.Command("pdftotext", "-", "-")
			cmd.Stdin = f
			output, err := cmd.CombinedOutput()
			if err != nil {
				msg := FirstLine(Truncate(string(output), 50))
				log.Printf("pdftotext %v: %v %v", fn, err, msg)
				return
			}
			// assess match, this may fail, as the title may not be written out
			// entirely in the source document (it may just be an author, volume
			// and issue, or other partial, but still identifying information)
			var (
				sourceDocLower     = strings.ToLower(string(output))
				targetTitleLower   = strings.ToLower(tr.Title)
				sourceDocLetters   = lettersOnly(norm.NFC.String(sourceDocLower))
				targetTitleLetters = lettersOnly(norm.NFC.String(targetTitleLower))
			)
			if !isAsciiOnly(targetTitleLetters) {
				log.Printf("[SK][🟡]skipping due to potential unicode issue: %v in %v", targetTitleLetters, fn)
				return
			}
			if strings.Contains(targetTitleLetters, "ff") {
				log.Printf("[SK][🟡]skipping due to potential ligature issue: %v in %v", targetTitleLetters, fn)
				return
			}
			if strings.Contains(sourceDocLetters, targetTitleLetters) {
				log.Printf("[OK][✅][%06d] found target title: %s", i.Load(), targetTitleLower)
				hits.Add(1)
			} else {
				log.Printf("[XX][❌][%06d] missing target title: \"%s\" in %s", i.Load(), targetTitleLower, fn)
			}
			i.Add(1)
			if i.Load() == *atLeastN {
				done = true
			}
		}()
		if sourceDocLink == "" {
			// log.Printf("skip: no url found")
			continue
		}
		h := sha1.New()
		_, _ = h.Write([]byte(sourceDocLink))
		hsum := fmt.Sprintf("%x", h.Sum(nil))
		// log.Printf("found link for source document link for %s: %v", sourceReleaseIdent, sourceDocLink)
		// need to find the target "title" in the text of the source document

		resp, err := client.Get(sourceDocLink)
		if err != nil {
			// log.Printf("skip: error downloading pdf: %v", err)
			continue
		}
		defer resp.Body.Close()

		// check if the request was successful (status code 200)
		if resp.StatusCode != http.StatusOK {
			// log.Printf("skip: unexpected status code %d\n", resp.StatusCode)
			continue
		}

		// cache the downloaded file
		targetFn := fmt.Sprintf("skate-eval-download-%s.pdf", hsum)
		targetPath := path.Join(os.TempDir(), targetFn)
		var tmpFile *os.File

		if _, err := os.Stat(targetPath); os.IsNotExist(err) {
			// create a temporary file to store the downloaded PDF content
			tmpFile, err = os.Create(targetPath)
			if err != nil {
				log.Fatalf("error creating temporary file: %v", err)
			}
			// copy the downloaded PDF content to the temporary file
			_, err = io.Copy(tmpFile, resp.Body)
			if err != nil {
				// log.Printf("skip: error copying PDF content to file: %v", err)
				continue
			}
			if _, err := tmpFile.Seek(0, io.SeekStart); err != nil {
				log.Fatal(err)
			}
		} else {
			tmpFile, err = os.Open(targetPath)
			if err != nil {
				log.Fatal(err)
			}
		}
		defer tmpFile.Close()

		// execute pdftotext command
		cmd := exec.Command("pdftotext", "-", "-") // "-" for stdin and "-" for stdout
		cmd.Stdin = tmpFile

		// capture the command output
		output, err := cmd.CombinedOutput()
		if err != nil {
			log.Fatalf("error executing pdftotext command: %v %v", err, string(output))
		}

		// assess match, this may fail, as the title may not be written out
		// entirely in the source document (it may just be an author, volume
		// and issue, or other partial, but still identifying information)
		sourceDocLower := strings.ToLower(string(output))
		targetTitleLower := strings.ToLower(tr.Title)

		// check if target title is in source document
		if strings.Contains(sourceDocLower, targetTitleLower) {
			log.Printf("[OK] found target title: %s", targetTitleLower)
			hits++
		} else {
			log.Printf("[XX] in: %s -- missing target title: \"%s\"", tmpFile.Name(), targetTitleLower)
			// maybe retry with smaller substrings
		}
		i++
	}
	pct := float64(hits.Load()) / float64(i.Load())
	log.Printf("[%d/%d][%0.2f]", hits.Load(), i.Load(), pct)
}
