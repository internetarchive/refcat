// skate-from-unstructured tries to parse various pieces of information from
// the unstructured field in refs.
package main

import (
	"flag"
	"log"
	"os"
	"runtime"

	"github.com/segmentio/encoding/json"
	"gitlab.com/internetarchive/refcat/skate"
	"gitlab.com/internetarchive/refcat/skate/parallel"
)

var (
	numWorkers = flag.Int("w", runtime.NumCPU(), "number of workers")
	batchSize  = flag.Int("b", 100000, "batch size")
)

func main() {
	pp := parallel.NewProcessor(os.Stdin, os.Stdout, func(p []byte) ([]byte, error) {
		var (
			ref skate.Ref
			err error
		)
		if err = json.Unmarshal(p, &ref); err != nil {
			return nil, err
		}
		if err = skate.ParseUnstructured(&ref); err != nil {
			return nil, err
		}
		return skate.JsonMarshalNewline(&ref)
	})
	pp.NumWorkers = *numWorkers
	pp.BatchSize = *batchSize
	if err := pp.Run(); err != nil {
		log.Fatal(err)
	}
}
