package skate

import "testing"

func TestSanitizeDOI(t *testing.T) {
	var cases = []struct {
		in  string
		out string
	}{
		{"", ""},
		{"a", ""},
		{"???", ""},
		{"10.1234", ""},
		{"10.1234/asdf ", "10.1234/asdf"},
		{"10.1234/ASDF", "10.1234/asdf"},
		{"10.1037/0002-9432.72.1.50", "10.1037/0002-9432.72.1.50"},
		{"http://doi.org/10.1234/asdf ", "10.1234/asdf"},
		{"http://doi.org/10.123", ""},
		{"dx.doi.org/10.1234/asdf ", "10.1234/asdf"},
		{"21924DOI10.1234/asdf ", "10.1234/asdf"},
		{"https://dx.doi.org/10.1234/asdf ", "10.1234/asdf"},
		{"doi:10.1234/asdf ", "10.1234/asdf"},
		{"10.7326/M20-6817", "10.7326/m20-6817"},
		// TODO: {"10.1037//0002-9432.72.1.50", "10.1037/0002-9432.72.1.50"},
	}
	for _, c := range cases {
		out := SanitizeDOI(c.in)
		if out != c.out {
			t.Fatalf("got %v, want %v", out, c.out)
		}
	}
}
