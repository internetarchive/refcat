module gitlab.com/internetarchive/refcat/skate

go 1.15

require (
	github.com/adrg/xdg v0.3.3
	github.com/benbjohnson/clock v1.3.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/klauspost/compress v1.16.7
	github.com/kr/pretty v0.3.1
	github.com/marcboeker/go-duckdb v1.6.1
	github.com/matryer/is v1.4.0
	github.com/mattn/go-sqlite3 v1.14.22 // indirect
	github.com/nsf/jsondiff v0.0.0-20210303162244-6ea32392771e
	github.com/segmentio/encoding v0.2.17
	github.com/sethgrid/pester v1.1.0
	github.com/tidwall/gjson v1.7.5
	golang.org/x/text v0.13.0
	mvdan.cc/xurls/v2 v2.3.0
)
