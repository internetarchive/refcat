package skate

import (
	"reflect"
	"sort"
	"testing"
)

func TestMatchSetLookup(t *testing.T) {
	var cases = []struct {
		s      string
		ps     []string
		n      int
		result []string
	}{
		{
			s:      "",
			ps:     []string{},
			n:      -1,
			result: nil,
		},
		{
			s:      "hello",
			ps:     []string{"lo"},
			n:      -1,
			result: nil,
		},
		{
			s:      "hello",
			ps:     []string{"zz"},
			n:      -1,
			result: nil,
		},
		{
			s:      "hello",
			ps:     []string{"h", "he", "hel"},
			n:      -1,
			result: nil,
		},
		{
			s:      "hello",
			ps:     []string{"hello"},
			n:      -1,
			result: []string{"hello"},
		},
		{
			s:      "hello world",
			ps:     []string{"hello", "world"},
			n:      -1,
			result: nil,
		},
		{
			s:      "he",
			ps:     []string{"hello", "world", "hel"},
			n:      -1,
			result: []string{"hello", "hel"},
		},
		{
			s: "american italian proceedings",
			ps: []string{
				"italian",
				"american",
				"american italian",
				"american italian proceedings",
				"proceedings",
			},
			n:      -1,
			result: []string{"american italian proceedings"},
		},
		{
			s: "american",
			ps: []string{
				"american proceedings XII",
				"american journal",
				"american",
			},
			n: -1,
			result: []string{
				"american proceedings XII",
				"american journal",
				"american",
			},
		},
	}
	for _, c := range cases {
		ms := NewMatchSet(c.ps)
		result := ms.Lookup(c.s, c.n)
		sort.Strings(result)
		sort.Strings(c.result)
		if !reflect.DeepEqual(result, c.result) {
			t.Errorf("got %v, want %v -> s=%s ps=%s",
				result, c.result, c.s, c.ps)
		}
	}
}
