# Try it out

Code example that work with the refcat citation graph, a reference dataset
released by the Internet Archive.

* Paper: [https://arxiv.org/abs/2110.06595](https://arxiv.org/abs/2110.06595)
* Blog: [Internet Archive Releases Refcat, the IA Scholar Index of over 1.3 Billion Scholarly Citations](https://blog.archive.org/2021/10/19/internet-archive-releases-refcat-the-ia-scholar-index-of-over-1-3-billion-scholarly-citations/)
* Code: [refcat](https://gitlab.com/internetarchive/refcat)
* Datasets: [v3](https://archive.org/details/refcat_2024-02-15), [v2](https://archive.org/details/refcat_2022-01-03), [v1](https://archive.org/details/refcat_2021-07-28)

## Format

Lastest release of refcat comes in two formats:

* biblioref JSON: [refcat-brefcombined-2024-02-15.json.zst](https://archive.org/download/refcat_2024-02-15/refcat-brefcombined-2024-02-15.json.zst) (162GB, compressed jsonlines)
* DOI table TSV: [refcat-doi-table-2024-02-15.tsv.zst](https://archive.org/download/refcat_2024-02-15/refcat-doi-table-2024-02-15.tsv.zst) (62GB, compressed TSV)

The tabular TSV file is easier to consume and contains 4 columns (source id, target id, source DOI, target DOI):

```
fngwj6...  t2x5fe...  10.1016/s0140-6736(79)91408-9   10.1001/archderm.112.2.256
xmb2by...  zij3r3...  10.2174/1874325001610010615     10.1001/2012.jama.11153
6onmyb...  ixqgtu...  10.1136/bmjopen-2021-056140     10.1001/amajethics.2020.116
tpvjsh...  ixqgtu...  10.1093/oncolo/oyac208          10.1001/amajethics.2020.116
```

The [refcat.py](refcat.py) script uses an indexed sqlite3 database version of
the source and target DOI, which makes queries really fast. The database is not
included in the Archive item, but can be created with a one liner using small
tool called [makta](https://github.com/miku/makta), which wraps import and
index generation.

```
$ wget https://archive.org/download/refcat_2024-02-15/refcat-doi-table-2024-02-15.tsv.zst
$ zstdcat refcat-doi-table-2024-02-15.tsv.zst | makta -o date-2024-02-20-doi-only.db
```

This process can take about two hours, but after that all queries come instantly.

After that, the refcat.py script can use the created database file to run
queries and to generate graphviz source code.

To list incoming edges:

```
$ python refcat.py --db date-2024-02-20-doi-only.db -R "10.17487/rfc3174" -m 1
10.4018/978-1-61520-783-1.ch003             10.17487/rfc3174
10.1109/iraset48871.2020.9092276            10.17487/rfc3174
10.4018/978-1-4666-5888-2.ch140             10.17487/rfc3174
10.4018/ijcac.2019040104                    10.17487/rfc3174
10.1016/j.adhoc.2010.05.005                 10.17487/rfc3174
10.1145/3152823                             10.17487/rfc3174
10.1007/s11276-017-1512-3                   10.17487/rfc3174
10.4028/www.scientific.net/amr.760-762.2129 10.17487/rfc3174
10.1145/2078216.2078228                     10.17487/rfc3174
10.4028/www.scientific.net/kem.597.131      10.17487/rfc3174
...
```

To generate graphviz source code:

```
digraph g {
 newrank=true; node [shape=box];
 node [shape=box,fontsize=8];
 rankdir=RL;
 "10.4018/978-1-7998-7705-9.ch028" [label="A Lightweight Authentication\nand Encryption Protocol for\nSecure Communications [...]\n[2021]"];
 "10.1016/j.comnet.2009.07.003" [label="Firewall policy verification\nand troubleshooting\n[2009]"];
 "10.1109/msr.2019.00031" [label="World of Code: An\nInfrastructure for Mining the\nUniverse of Open Source [...]\n[2019]"];
 "10.1007/978-0-387-74115-4_2" [label="Background\n[2007]"];
 "10.1145/3032990" [label="A Canonical Form for PROV\nDocuments and Its Application\nto Equality, Signature, [...]\n[2017]"];
 "10.1145/964001.964018" [label="A semantics for web services\nauthentication\n[2004]"];
 ...
 "10.1016/j.future.2017.03.017" -> "10.17487/rfc3174";
 "10.1109/eurospw51379.2020.00056" -> "10.17487/rfc3174";
 "10.1016/j.peva.2018.09.004" -> "10.17487/rfc3174";
 "10.1186/s12920-020-0712-3" -> "10.17487/rfc3174";
}
```

To generate a diagram:

```
$ python refcat.py --db date-2024-02-20-doi-only.db -R "10.17487/rfc3174" -m 1 -d | \
    dot -T png > rfc3174-1.png
```

![](rfc3174-1-screenshot.png)

You can adjust the recursion depth with the `-m` flag. If there are more than
200 nodes, a random sample of the incoming edges is taken.

```
$ python refcat.py --db date-2024-02-20-doi-only.db -R "10.17487/rfc3174" -m 2 -d | \
    dot -T png > rfc3174-2.png
```

![](rfc3174-2-screenshot.png)


### Format: biblioref

Serialized as JSON containing fields relevant to further (internal) processing.

```json
{
  "_id": "z4zgngpuujcc7bu2sefswpfn6q_5",
  "indexed_ts": "2022-01-15T10:17:28Z",
  "source_release_ident": "z4zgngpuujcc7bu2sefswpfn6q",
  "source_work_ident": "22222dgdnzgxpmeq77nyyuj2x4",
  "source_release_stage": "published",
  "source_year": "2005",
  "ref_index": 5,
  "ref_key": "bib5",
  "target_release_ident": "dnntmhwmfzapneygeacbeejhvm",
  "target_work_ident": "lyvdqgn6gjgnlptrvd4edgz3nu",
  "match_provenance": "crossref",
  "match_status": "exact",
  "match_reason": "doi"
}
```

