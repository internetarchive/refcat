#!/usr/bin/env python

"""

Recursively return inbound edges:

    with R as
      (select k as key,
              v as value,
              1 as level
       FROM map
       where v = '10.1111/j.1559-1816.2009.00546.x'
       UNION all select distinct key,
                                 map.v,
                                 R.level + 1
       from R
       join map on R.key = map.v
       where R.level < 2)
    select *
    from R;

# TODO

* [ ] reduce number of edges
* [ ] focus on largest connected component
* [ ] background color, based on degree count of node
* [ ] cluster by year
* [ ] label with title, year and doi

"""

from typing import List, Dict, Iterable, Optional, Any
import argparse
import collections
import datetime
import errno
import hashlib
import io
import itertools
import json
import os
import random
import requests
import six
import sqlite3
import string
import sys
import tempfile
import textwrap

import backoff

Edge = collections.namedtuple("Edge", "s t depth")  # source, target, number of hops from starting node

SQL_RECURSIVE_EDGE_SELECT = """
with R as
  (select k as key,
          v as value,
          1 as level
   FROM map
   where v = ?
   UNION all select k,
                             map.v,
                             R.level + 1
   from R
   join map on R.key = map.v
   where R.level < ?)
select distinct *
from R;
"""

class URLCache:
    """
    A simple URL content cache. Stores everything on the filesystem. Content is
    first written to a temporary file and then renamed. With concurrent
    requests for the same URL, the last one wins (LOW). Raises exception on any
    HTTP status >= 400. Retries supported.

    It is not very efficient, as it creates lots of directories.
    > 396140 directories, 334024 files ... ...

    To clean the cache just remove the cache directory.

    >>> cache = URLCache()
    >>> cache.get_cache_file("https://www.google.com")
    /tmp/ef/7e/fc/ef7efc9839c3ee036f023e9635bc3b056d6ee2d

    >>> cache.is_cached("https://www.google.com")
    False

    >>> page = cache.get("https://www.google.com")
    >>> page[:15]
    '<!doctype html>'

    >>> cache.is_cached("https://www.google.com")
    True

    It is possible to force a download, too:

    >>> page = cache.get("https://www.google.com", force=True)
    """

    def __init__(self, directory=None, max_tries=12):
        """
        If `directory` is not explictly given, all files will be stored under
        the temporary directory. Requests can be retried, if they resulted in
        a non 200 HTTP status code. A server might send a HTTP 500 (Internal
        Server Error), even if it really is a HTTP 503 (Service Unavailable).
        We therefore treat HTTP 500 errors as something to retry on,
        at most `max_tries` times.
        """
        self.directory = directory or tempfile.gettempdir()
        self.sess = requests.session()
        self.max_tries = max_tries

    def get_cache_file(self, url):
        """
        Return the cache file path for a URL. This will - as a side effect -
        create the parent directories, if necessary.
        """
        digest = hashlib.sha1(six.b(url)).hexdigest()
        d0, d1, d2 = digest[:2], digest[2:4], digest[4:6]
        path = os.path.join(self.directory, d0, d1, d2)

        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError as e:
                if e.errno == errno.EEXIST:
                    pass
                else:
                    raise
        return os.path.join(path, digest)

    def is_cached(self, url):
        return os.path.exists(self.get_cache_file(url))

    def get(self, url, force=False, ttl_seconds=None):
        """
        Return URL, either from cache or the web. With `force` get will always
        re-download a URL. Use `ttl_seconds` to set a TTL in seconds (day=86400,
        month=2592000, six month=15552000, a year=31104000).
        """

        def is_ttl_expired(url):
            """
            Returns True, if modification date of the file lies befores TTL.
            """
            if ttl_seconds is None:
                return False
            mtime = datetime.datetime.fromtimestamp(os.path.getmtime(self.get_cache_file(url)))
            xtime = datetime.datetime.now() - datetime.timedelta(seconds=ttl_seconds)
            is_expired = mtime < xtime
            return is_expired

        @backoff.on_exception(backoff.expo, RuntimeError, max_tries=self.max_tries)
        def fetch(url):
            """
            Nested function, so we can configure number of retries.
            """
            r = self.sess.get(url, timeout=600)
            if r.status_code >= 400:
                raise RuntimeError("%s on %s" % (r.status_code, url))
            with tempfile.NamedTemporaryFile(delete=False) as output:
                output.write(r.text.encode("utf-8"))
            os.rename(output.name, self.get_cache_file(url))

        if not self.is_cached(url) or force is True or is_ttl_expired(url):
            fetch(url)

        with open(self.get_cache_file(url)) as handle:
            return handle.read()

class Graph:
    """
    Slight abtraction over some graph operations. Wraps an sqlite3 database
    with source and target DOI as columns.
    """

    def __init__(
        self,
        db: str = "date-2024-02-20-doi-only.db",
        fatcat_api: str = "https://api.fatcat.wiki/v0",
        crossref_api: str = "https://api.crossref.org/",
    ) -> None:
        self.con = sqlite3.connect(db)

    def recursive_edges(self, value: str, inbound: bool = True, max_depth: int = 1) -> List[Edge]:
        """
        This is inbound only, for now.
        """
        rows = self.con.execute(SQL_RECURSIVE_EDGE_SELECT, (value, max_depth)).fetchall()
        return [Edge(*row) for row in rows]

    # deprecated
    def collect_edges_recursive(self, value: str, inbound: bool = False, max_depth: int = 1) -> List[Edge]:
        depth, result = 0, set()
        queue = [value]
        while queue and depth < max_depth:
            w = queue.pop()
            for edge in collect_edges(w, inbound=inbound):
                result.add(edge)
                if inbound:
                    queue.append(edge.s)
                else:
                    queue.append(edge.t)
            depth += 1
        return result

    # deprecated
    def collect_edges(self, value: str, inbound: bool = False) -> List[Edge]:
        if inbound:
            rows = self.con.execute("select * from map where v = ?", (value,)).fetchall()
        else:
            rows = self.con.execute("select * from map where k = ?", (value,)).fetchall()
        return [Edge(*row) for row in rows]


def batched(iterable, n):
    # batched('ABCDEFG', 3) --> ABC DEF G
    if n < 1:
        raise ValueError("n must be at least one")
    it = iter(iterable)
    while batch := tuple(itertools.islice(it, n)):
        yield batch


def create_short_name_map(vs: Iterable[str]) -> Dict[str, str]:
    mnemonics = list("".join(v) for v in itertools.combinations(string.ascii_uppercase + string.digits, r=6))
    random.shuffle(mnemonics)
    return dict(zip(vs, mnemonics))

def fetch_node_metadata(node: str, cache : Optional[URLCache]=None) -> Dict[str, Any]:
    # node is the DOI
    # link = f"{crossref_api}/works/{node}"
    link = f"https://api.fatcat.wiki/v0/release/lookup?doi={node}"
    # print(link, file=sys.stderr)
    data = json.loads(cache.get(link)) # may fail
    # print(data, file=sys.stderr)
    return data




def edges_to_dot(edges: List[Edge]) -> str:
    names = set(e.s for e in edges).union(e.t for e in edges)
    mdmap = {} # map from name to node metadata
    cache = URLCache()
    for name in names:
        data = fetch_node_metadata(name, cache=cache)
        mdmap[name] = data

    # names are all the "DOI"
    short_name_map = create_short_name_map(names)
    sio = io.StringIO()
    print("digraph g {", file=sio)
    print(""" newrank=true; node [shape=box]; """, file=sio)
    print(""" node [shape=box,fontsize=8];  """, file=sio)
    print(""" rankdir=LR; """, file=sio)

    for name in names:
        md = mdmap.get(name)
        title = md.get("title", "NA")
        title = title.replace('"', "")
        lines = textwrap.wrap(title, width=30, max_lines=3)
        year = md.get("release_year", "NA")
        lines.append(f"[{year}]")
        label = "\\n".join(lines)
        print(""" "{}" [label="{}"];  """.format(name, label), file=sio)

    # for batch in batched(names, 4):
    #     quoted = ['"{}"'.format(b) for b in batch]
    #     print(
    #         """ {{ rank = same; node [shape=box]; {}; }}""".format("; ".join(quoted)),
    #         file=sio,
    #     )

    for i, edge in enumerate(edges):
        s = edge.s  # short_name_map[edge.s]
        t = edge.t  # short_name_map[edge.t]
        print(f""" "{s}" -> "{t}";""", file=sio)
    print("}", file=sio)
    return sio.getvalue()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--db", default="date-2024-02-20-doi-only.db", type=str, help="sqlite3 file")
    parser.add_argument("-R", type=str, help="inbound edges to given DOI")
    parser.add_argument("-d", action="store_true", help="write dot to stdout")
    parser.add_argument("-f", type=str, help="outbound edges from given DOI")
    parser.add_argument("-t", type=str, help="inbound edges to given DOI")
    parser.add_argument("-m", type=int, default=1, help="max depth to recurse")
    parser.add_argument("--show-info", action="store_true", help="show info about the graph")

    args = parser.parse_args()
    graph = Graph(db=args.db)

    if args.show_info:
        result = con.execute("select count(*) from map").fetchone()
        print(result[0])

    if args.R:
        edges = graph.recursive_edges(args.R, inbound=True, max_depth=3)
        if len(edges) > 200:
            edges = random.sample(edges, 200) # be better

    if args.d:
        print(edges_to_dot(edges))

    if args.f:
        edges = graph.collect_edges_recursive(con, args.f, inbound=False, max_depth=args.m)
        if args.d:
            print(edges_to_dot(edges))
        else:
            for edge in edges:
                print(f"{edge.s}\t{edge.t}")
    if args.t:
        edges = graph.collect_edges_recursive(con, args.t, inbound=True, max_depth=args.m)
        if args.d:
            print(edges_to_dot(edges))
        else:
            for edge in edges:
                print(f"{edge.s}\t{edge.t}")


