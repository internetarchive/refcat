package skate

import (
	"strings"
)

// slugifyString is a basic string slugifier.
func slugifyString(s string) string {
	var sb strings.Builder
	for _, c := range strings.TrimSpace(strings.ToLower(s)) {
		if (c > 96 && c < 123) || (c > 47 && c < 58) || (c == 32) || (c == 9) || (c == 10) {
			_, _ = sb.WriteRune(c)
		}
	}
	return strings.Join(strings.Fields(sb.String()), " ")
}
