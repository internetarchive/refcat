package skate

import "testing"

func TestSlugifyString(t *testing.T) {
	var cases = []struct {
		s      string
		result string
	}{
		{"", ""},
		{" ", ""},
		{" Optimize everything", "optimize everything"},
		{"ABCü~", "abc"},
	}
	for _, c := range cases {
		got := slugifyString(c.s)
		if got != c.result {
			t.Errorf("slugifyString: '%v', want '%v', got '%v'", c.s, c.result, got)
		}
	}
}
