# Test data for reducers

Format:

> c[F][ID][a|b|r].[json|out]

where F relates to the function being tested, ID is a test case id, a and b are
input streams, and r is the result output.

Possible F:

| Mnemonic  | Function                      |
|---------- |----------------------------   |
| E         | ZippyExact                    |
| R         | ZippyExactRelease             |
| W         | ZippyExactWiki                |
| V         | ZippyVerifyRefs               |
| O         | ZippyVerifyRefsOpenLibrary    |
| B         | ZippyBrefAugment              |

Example:

* inputs: cE17a.json, cE17b.json; output: cE17r.out

