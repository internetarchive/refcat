package skate

import (
	"regexp"
)

var (
	PatDOI         = regexp.MustCompile(`10[.][0-9]{1,8}/[^ ]*[\w]`)
	PatDOINoHyphen = regexp.MustCompile(`10[.][0-9]{1,8}/[^ -]*[\w]`)
	PatArxiv       = regexp.MustCompile(`https?://arxiv.org/(pdf|abs)/([0-9]{4,4}[.][0-9]{1,8})(v[0-9]{1,2})?(.pdf)?`)
)

// ParseUnstructured will in-place augment missing DOI, arxiv id and so on.
func ParseUnstructured(ref *Ref) error {
	var (
		uns = ref.Biblio.Unstructured
		v   string
		vs  []string
	)
	// DOI
	v = PatDOI.FindString(uns)
	if v != "" && ref.Biblio.DOI == "" {
		ref.Biblio.DOI = SanitizeDOI(v)
	}
	// Arxiv
	vs = PatArxiv.FindStringSubmatch(uns)
	if len(vs) != 0 && ref.Biblio.ArxivId == "" {
		ref.Biblio.ArxivId = vs[2]
	}
	// XXX: ISBN
	ref.Biblio.Extra.ISBN = ParseIsbn(uns)
	// Some more, not yet handled examples:
	// - 2005 Sep;95(9):1545-51
	// - Pattern Anal. Mach. Intell., 36(2):346-360, 2014. 4, 6, 7"
	// - only unstructured, and it contains a URL - then reference might be to
	//   that site, actually, e.g.
	//   https://fatcat.wiki/release/xqgaanhpf5gotdxxxytgyxw2ty/references ref #20
	//
	//
	// There seems to be a number tiny of data issues; e.g. one off dates. Can
	// we fix them here?
	// Include: https://images.webofknowledge.com/images/help/WOS/J_abrvjt.html, ...
	//
	// Also: We may have differing values (between .pages and some hint in
	// unstructured), e.g. for the page counts, etc.

	return nil
}
