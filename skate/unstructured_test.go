package skate

import (
	"reflect"
	"testing"
)

func TestParseUnstructured(t *testing.T) {
	// XXX: add more cases, maybe move this into files.
	var cases = []struct {
		ref    *Ref
		result *Ref
		err    error
	}{
		{
			&Ref{
				Biblio: Biblio{
					Unstructured: "Hello 10.1111/j.1550-7408.1968.tb02138.x-BIB5",
				},
			},
			&Ref{
				Biblio: Biblio{
					DOI:          "10.1111/j.1550-7408.1968.tb02138.x-bib5",
					Unstructured: "Hello 10.1111/j.1550-7408.1968.tb02138.x-BIB5",
				},
			},
			nil,
		},
		{
			&Ref{
				Biblio: Biblio{
					Unstructured: "https://arxiv.org/pdf/0808.3320v3.pdf Hello 10.1111/j.1550-7408.1968.tb02138.x-BIB5",
				},
			},
			&Ref{
				Biblio: Biblio{
					ArxivId:      "0808.3320",
					DOI:          "10.1111/j.1550-7408.1968.tb02138.x-bib5",
					Unstructured: "https://arxiv.org/pdf/0808.3320v3.pdf Hello 10.1111/j.1550-7408.1968.tb02138.x-BIB5",
				},
			},
			nil,
		},
	}
	for _, c := range cases {
		err := ParseUnstructured(c.ref)
		if err != c.err {
			t.Fatalf("got %v, want %v", err, c.err)
		}
		if !reflect.DeepEqual(c.ref, c.result) {
			t.Fatalf("got %#v, want %#v", c.ref, c.result)
		}
	}
}

func BenchmarkParseUnstructured(b *testing.B) {
	ref := Ref{
		Biblio: Biblio{
			Unstructured: "https://arxiv.org/pdf/0808.3320v3.pdf Hello 10.1111/j.1550-7408.1968.tb02138.x-BIB5",
		},
	}
	for n := 0; n < b.N; n++ {
		ParseUnstructured(&ref)
	}
}
