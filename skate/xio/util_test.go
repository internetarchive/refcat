package xio

import (
	"io/ioutil"
	"testing"
)

func TestOpenTwo(t *testing.T) {
	a, err := tempFile(t)
	if err != nil {
		t.Fatalf("failed to create tmp file: %v", err)
	}
	b, err := tempFile(t)
	if err != nil {
		t.Fatalf("failed to create tmp file: %v", err)
	}
	f1, f2, err := OpenTwo(a, b)
	if err != nil {
		t.Fatalf("failed to OpenTwo: %v", err)
	}
	defer f1.Close()
	defer f2.Close()
}

// tempFile creates an empty temporary file and returns its name on success.
func tempFile(t *testing.T) (filename string, err error) {
	f, err := ioutil.TempFile(t.TempDir(), "")
	if err != nil {
		return "", err
	}
	if err := f.Close(); err != nil {
		return "", err
	}
	return f.Name(), nil
}
